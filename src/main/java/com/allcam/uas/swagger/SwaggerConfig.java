package com.allcam.uas.swagger;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/** * SwaggerConfig */
/**
 * 访问http://localhost:8080/swagger-ui.html 就能看到<br/>
 * SpringBoot之springfox(Swagger) (ApiDoc接口文档)
 * 
 * @author XiongJinTeng
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig
{
    
    /**
     * * SpringBoot默认已经将classpath:/META-INF/resources/和classpath:/META-INF/ resources/webjars/映射 *
     * 所以该方法不需要重写，如果在SpringMVC中，可能需要重写定义（我没有尝试） * 重写该方法需要 extends WebMvcConfigurerAdapter *
     */
    // @Override
    // public void addResourceHandlers(ResourceHandlerRegistry registry) {
    // registry.addResourceHandler("swagger-ui.html")
    // .addResourceLocations("classpath:/META-INF/resources/");
    //
    // registry.addResourceHandler("/webjars/**")
    // .addResourceLocations("classpath:/META-INF/resources/webjars/");
    // }
    
    /** * 可以定义多个组，比如本类中定义把test和demo区分开了 * （访问页面就可以看到效果了） * */
    @Bean
    public Docket testApi()
    {
        return new Docket(DocumentationType.SWAGGER_2).groupName("uas").genericModelSubstitutes(DeferredResult.class)
        // .genericModelSubstitutes(ResponseEntity.class)
            .useDefaultResponseMessages(false)
            .forCodeGeneration(true)
            .pathMapping("/")
            // base，最终调用接口后会和paths拼接在一起
            .select()
            .paths(or(regex("/api/.*")))
            // 过滤的接口
            .build()
            .apiInfo(apiInfo());
    }
    
    private ApiInfo apiInfo()
    {
        Contact contact = new Contact("奥看科技", "http://www.allcam.com.cn", "service@allcam.com.cn");
        ApiInfo apiInfo = new ApiInfo("租户管理子系统API接口", // 大标题
            "REST风格API", // 小标题
            "v0.0.1", // 版本
            "www.allcam.com.cn", contact, // 作者
            "主页", // 链接显示文字
            "http://uas.allcam.com.cn/api/"// 网站链接
        );
        return apiInfo;
    }
}