/**
 * 
 */
package com.allcam.uas.sys.exception;

/**
 * 数据库异常封装类。包装数据库操作过程中出现的各种异常错误。
 * 
 * @author yizhichao
 * @version 1.0 2006-9-11
 * @see AdapterException
 * @since 1.0
 */
public class DBException extends Exception
{
    
    /**
     * 序列化编号
     */
    private static final long serialVersionUID = -4061688658940721638L;
    
    /**
     * 带参构造
     * 
     * @param strMsg 待记录的异常消息
     */
    public DBException(String strMsg)
    {
        super(strMsg);
    }
    
    /**
     * 带参构造
     * 
     * @param ex 传入的异常类实例
     */
    public DBException(Throwable ex)
    {
        super(ex);
    }
    
    /**
     * 带参构造
     * 
     * @param strMsg 待记录的异常消息
     * @param ex 传入的异常类实例
     */
    public DBException(String strMsg, Throwable ex)
    {
        super(strMsg, ex);
    }
    
    /**
     * 重写
     * 
     * @return 返回该类实例的字符串表示
     */
    public String toString()
    {
        return super.toString();
    }
}
