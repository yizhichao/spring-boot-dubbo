package com.allcam.uas.common.httpbeans.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 消息响应的结果码
 * 
 * @author  marui
 * @version  [版本号, 2015-3-6]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result  implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -922329394733077237L;

    /**
     * 成功
     */
    public static final String SUCCESS = "1000";
    
    /**
     * 鉴权失败
     */
    public static final String AUTH_FAIL = "1001";
    
    /**
     * 结果码
     */
    private String resultCode;
    
    /**
     * 结果描述
     */
    private String resultDesc;
    
    public String getResultCode()
    {
        return resultCode;
    }
    
    public void setResultCode(String resultCode)
    {
        this.resultCode = resultCode;
    }
    
    public String getResultDesc()
    {
        return resultDesc;
    }
    
    public void setResultDesc(String resultDesc)
    {
        this.resultDesc = resultDesc;
    }
}
