package com.allcam.uas.common.httpbeans.base;

import java.io.Serializable;

import com.allcam.uas.common.httpbeans.AcBaseBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 消息体的头信息
 * 
 * @author marui
 * @version [版本号, 2015-3-6]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MsgHead extends AcBaseBean implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 7873791230401553314L;
    
    /**
     * 消息版本号
     */
    private String version;
    
    /**
     * 消息方向：request,response
     */
    private String direction;
    
    /**
     * 消息类型
     */
    private String msgType;
    
    /**
     * 语言环境 Zh : 中文 En : 英文
     */
    private String userLan;
    
    /**
     * 客户端类型
     */
    private String cuType;
    
    /**
     * 客户端程序版本
     */
    private String cuVersion;
    
    /**
     * 客户端程序版本描述
     */
    private String cuVersionDesc;
    
    /**
     * 客户端操作系统版本
     */
    private String systemVersion;
    
    @JsonIgnore
    private String userNameAuth;
    
    public String getUserNameAuth()
    {
        return userNameAuth;
    }
    
    public void setUserNameAuth(String userNameAuth)
    {
        this.userNameAuth = userNameAuth;
    }
    
    public String getVersion()
    {
        return version;
    }
    
    public void setVersion(String version)
    {
        this.version = version;
    }
    
    public String getDirection()
    {
        return direction;
    }
    
    public void setDirection(String direction)
    {
        this.direction = direction;
    }
    
    public String getMsgType()
    {
        return msgType;
    }
    
    public void setMsgType(String msgType)
    {
        this.msgType = msgType;
    }
    
    public String getUserLan()
    {
        return userLan;
    }
    
    public void setUserLan(String userLan)
    {
        this.userLan = userLan;
    }
    
    public String getCuType()
    {
        return cuType;
    }
    
    public void setCuType(String cuType)
    {
        this.cuType = cuType;
    }
    
    public String getCuVersion()
    {
        return cuVersion;
    }
    
    public void setCuVersion(String cuVersion)
    {
        this.cuVersion = cuVersion;
    }
    
    public String getCuVersionDesc()
    {
        return cuVersionDesc;
    }
    
    public void setCuVersionDesc(String cuVersionDesc)
    {
        this.cuVersionDesc = cuVersionDesc;
    }
    
    public String getSystemVersion()
    {
        return systemVersion;
    }
    
    public void setSystemVersion(String systemVersion)
    {
        this.systemVersion = systemVersion;
    }
}
