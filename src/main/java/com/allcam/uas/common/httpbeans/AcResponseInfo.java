package com.allcam.uas.common.httpbeans;

import java.io.Serializable;

import com.allcam.uas.common.httpbeans.base.MsgHead;
import com.allcam.uas.common.httpbeans.base.Result;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * HTTP响应对象
 * 
 * @author marui
 * @version [版本号, 2014-5-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler", "operations", "roles", "menus" })
public class AcResponseInfo extends AcBaseBean implements Serializable {
	/**
	 * 注释内容
	 */
	private static final long serialVersionUID = -610014338197639408L;

	@JsonProperty(value = "MsgHead")
	protected MsgHead msgHead;

	@JsonProperty(value = "Result")
	protected Result result;

	public MsgHead getMsgHead() {
		return msgHead;
	}

	public void setMsgHead(MsgHead msgHead) {
		this.msgHead = msgHead;
	}

	public Result getResult() {
		if (null == result) {
			Result resultNew = new Result();
			this.result = resultNew;
		}
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

}
