package com.allcam.uas.common.httpbeans.base;

import java.io.Serializable;

import com.allcam.uas.common.httpbeans.AcBaseBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 消息体的主信息
 * 
 * @author marui
 * @version [版本号, 2015-3-6]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MsgBody extends AcBaseBean implements Serializable
{
    
    /**
     * 
     */
    private static final long serialVersionUID = -1863233259207257461L;
    
    @JsonIgnore
    private String userNameAuth;
    
    public String getUserNameAuth()
    {
        return userNameAuth;
    }
    
    public void setUserNameAuth(String userNameAuth)
    {
        this.userNameAuth = userNameAuth;
    }
}
