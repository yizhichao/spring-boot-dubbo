package com.allcam.uas.common.httpbeans;

import java.io.Serializable;

import com.allcam.uas.common.httpbeans.base.MsgHead;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * HTTP请求对象
 * 
 * @author marui
 * @version [版本号, 2014-5-26]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AcRequestInfo extends AcBaseBean implements Serializable {
	/**
	 * 注释内容
	 */
	private static final long serialVersionUID = 6209193398717417710L;

	@JsonProperty(value = "MsgHead")
	protected MsgHead msgHead;

	public MsgHead getMsgHead() {
		return msgHead;
	}

	public void setMsgHead(MsgHead msgHead) {
		this.msgHead = msgHead;
	}
}
