package com.allcam.uas.common.httpbeans;

import com.allcam.utils.Tools;

public abstract class AcBaseBean
{
    @Override
    public String toString()
    {
        return Tools.beanToString(this);
    }
}
