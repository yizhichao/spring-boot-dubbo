package com.allcam.uas.modules.dev.controller;

import io.swagger.annotations.Api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/** * DemoController * */
@Api(description = "设备管理")
@Controller
@RequestMapping(value = "/api/dev")
public class DevController
{
    
    private Logger logger = LoggerFactory.getLogger(DevController.class);
    
    /**
     * * 可以直接使用@ResponseBody响应JSON * * @param request * @param response * @return
     */
    @ResponseBody
    @RequestMapping(value = "/create", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> create(HttpServletRequest request, HttpServletResponse response)
    {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        return list;
    }
    
    @ResponseBody
    @RequestMapping(value = "/update", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> update(HttpServletRequest request, HttpServletResponse response)
    {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        return list;
    }
    
    @ResponseBody
    @RequestMapping(value = "/get", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> get(HttpServletRequest request, HttpServletResponse response)
    {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        return list;
    }
    
    @ResponseBody
    @RequestMapping(value = "/delete", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> delete(HttpServletRequest request, HttpServletResponse response)
    {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        return list;
    }
    
    @ResponseBody
    @RequestMapping(value = "/set", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> set(HttpServletRequest request, HttpServletResponse response)
    {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        return list;
    }
}