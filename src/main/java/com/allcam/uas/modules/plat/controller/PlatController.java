package com.allcam.uas.modules.plat.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/** * DemoController * */
@Controller
@RequestMapping(value = "/api/plat")
public class PlatController
{
    
    private Logger logger = LoggerFactory.getLogger(PlatController.class);
    
    /**
     * * 可以直接使用@ResponseBody响应JSON * * @param request * @param response * @return
     */
    @ResponseBody
    @RequestMapping(value = "/get", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> get(HttpServletRequest request, HttpServletResponse response)
    {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        return list;
    }
}