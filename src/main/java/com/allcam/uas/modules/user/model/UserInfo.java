package com.allcam.uas.modules.user.model;

public class UserInfo
{
    
    private int id;
    
    private String userName;
    
    private String passWord;
    
    private String sex;
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public String getUserName()
    {
        return userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getPassWord()
    {
        return passWord;
    }
    
    public void setPassWord(String passWord)
    {
        this.passWord = passWord;
    }
    
    public String getSex()
    {
        return sex;
    }
    
    public void setSex(String sex)
    {
        this.sex = sex;
    }
    
    @Override
    public String toString()
    {
        return "UserInfo [id=" + id + ", userName=" + userName + ", passWord=" + passWord + ", sex=" + sex + "]";
    }
}
