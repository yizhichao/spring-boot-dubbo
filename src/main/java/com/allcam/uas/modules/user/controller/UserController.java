package com.allcam.uas.modules.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import springfox.documentation.annotations.ApiIgnore;

import com.alibaba.fastjson.JSONObject;
import com.allcam.modules.demo.inf.DemoService;
import com.allcam.uas.modules.user.model.UserInfo;
import com.allcam.uas.sys.init.SystemContextListener;

/** * DemoController * */
@Api(description = "用户管理")
@Controller
@RequestMapping(value = "/api/user")
public class UserController
{
    
    private Logger logger = LoggerFactory.getLogger(UserController.class);
    
    /**
     * * 可以直接使用@ResponseBody响应JSON * * @param request * @param response * @return
     */
    @ApiIgnore
    // 使用该注解忽略这个API
    @ResponseBody
    @RequestMapping(value = "/create", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelMap create(HttpServletRequest request, HttpServletResponse response)
    {
        ModelMap map = new ModelMap();
        map.addAttribute("hello", "你好");
        map.addAttribute("veryGood", "很好");
        
        return map;
    }
   
    /**
     * * 可以直接使用@ResponseBody响应JSON * * @param request * @param response * @return
     */
    @ResponseBody
    @RequestMapping(value = "/get", method = {RequestMethod.POST, RequestMethod.GET})
    public List<String> get(HttpServletRequest request, HttpServletResponse response)
    {
    	System.out.println(request.getAttribute("jsonData"));
        List<String> list = new ArrayList<String>();
        list.add("hello");
        list.add("你好");
        WebApplicationContext  context =(WebApplicationContext)SystemContextListener.applicationContext;
        DemoService demoService = (DemoService)context.getBean("demoService"); // 获取远程服务代理
        for (int i = 0; i < 1; i++)
        {
            String hello = demoService.sayHello("world"); // 执行远程方法
            System.out.println(hello + "2"  +i);
        }
        return list;
    }
    
    /**
     * * JSON请求一个对象<br/>
     * * （Ajax Post Data：{"name":"名称","content":"内容"}） * * @param version * @return
     */
    @ResponseBody
    @RequestMapping(value = "/info", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelMap info(@RequestBody UserInfo userInfo)
    {
        logger.info("userName：" + userInfo.getUserName());
        logger.info("passWord：" + userInfo.getPassWord());
        ModelMap map = new ModelMap();
        map.addAttribute("result", "ok");
        return map;
    }
    
}