package com.allcam.uas.modules.user.model;

import java.io.Serializable;

import com.allcam.uas.common.httpbeans.AcRequestInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfoReq extends AcRequestInfo
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -3385026155181973421L;
    
    /**
     * 注释内容
     */
    @JsonProperty(value = "MsgBody")
    private MsgBody msgBody;
    
    public MsgBody getMsgBody()
    {
        if (null == msgBody)
        {
            return new MsgBody();
        }
        return msgBody;
    }
    
    public void setMsgBody(MsgBody msgBody)
    {
        this.msgBody = msgBody;
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class MsgBody implements Serializable
    {
        /**
         * 注释内容
         */
        private static final long serialVersionUID = -7235768237728322621L;
        
        /**
         * String Y 0：ECSHOP 网店 1：OA 2：BDGW 考勤 3：积分商城（全量兑换商品列表） 4：积分商城（具体某个活动界面） -- 积分 5：线上活动（优秀成长记）
         */
        private String sysType;//
        
        /**
         * String N sysType为 4 、 5的时候 带上平台活动数据 必要数据
         */
        private String sysData;//
        
        public String getSysType()
        {
            return sysType;
        }
        
        public void setSysType(String sysType)
        {
            this.sysType = sysType;
        }
        
        public String getSysData()
        {
            return sysData;
        }
        
        public void setSysData(String sysData)
        {
            this.sysData = sysData;
        }
        
        @Override
        public String toString()
        {
            return "MsgBody [sysType=" + sysType + ", sysData=" + sysData + "]";
        }
    }
}