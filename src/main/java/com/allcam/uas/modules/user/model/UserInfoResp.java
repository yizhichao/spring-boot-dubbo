package com.allcam.uas.modules.user.model;

import java.io.Serializable;

import com.allcam.uas.common.httpbeans.AcResponseInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler", "operations", "roles", "menus"})
public class UserInfoResp extends AcResponseInfo
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -3480500194437586712L;
    
    @JsonProperty(value = "MsgBody")
    private MsgBody msgBody;
    
    public MsgBody getMsgBody()
    {
        return msgBody;
    }
    
    public void setMsgBody(MsgBody msgBody)
    {
        this.msgBody = msgBody;
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MsgBody implements Serializable
    {

        /**
         * 注释内容
         */
        private static final long serialVersionUID = 7161856687106536009L;
        
        
        private String ssoSysUrl;// String Y SSO单点登录地址
        
        public String getSsoSysUrl()
        {
            return ssoSysUrl;
        }
        
        public void setSsoSysUrl(String ssoSysUrl)
        {
            this.ssoSysUrl = ssoSysUrl;
        }

        @Override
        public String toString()
        {
            return "MsgBody [ssoSysUrl=" + ssoSysUrl + "]";
        }
    }
}
