package com.allcam.uas.filter;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.allcam.adapter.common.bean.MsgheadInfo;
import com.allcam.common.ServiceContants;
import com.allcam.uas.common.SystemConstant;
import com.allcam.uas.sys.exception.CustomException;
import com.allcam.utils.RandomUtil;
import com.allcam.utils.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter implements ServiceContants
{
    
    private Logger logger = LoggerFactory.getLogger(WebMvcConfigurer.class);
    
    private static final List<String> OPEN_RESOURCE = new ArrayList<String>();
    
    public static final Map<String, String> NONCE_MAP = new HashMap<String, String>();
    
    public boolean isAuth(String url)
    {
        for (String string : OPEN_RESOURCE)
        {
            if (url.contains(string))
            {
                return true;
            }
        }
        return false;
    }
    
    static
    {
        OPEN_RESOURCE.add("jsp");
        OPEN_RESOURCE.add("js");
        OPEN_RESOURCE.add("css");
        OPEN_RESOURCE.add("images");
        OPEN_RESOURCE.add("backRes");
        OPEN_RESOURCE.add("anon");
        OPEN_RESOURCE.add("ckplayer");
        OPEN_RESOURCE.add("htm");
        OPEN_RESOURCE.add("html");
        OPEN_RESOURCE.add("ico");
        OPEN_RESOURCE.add("uploadImage");
        OPEN_RESOURCE.add("swagger-ui.html");
        OPEN_RESOURCE.add("/swagger-resources/configuration/ui");
        OPEN_RESOURCE.add("/swagger-resources");
        OPEN_RESOURCE.add("/v2/api-docs");
    }
    
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(new HandlerInterceptorAdapter()
        {
            
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
                throws Exception
            {
                boolean result = true;
                request.setCharacterEncoding(SystemConstant.UTF8_CODE);
                response.setCharacterEncoding(SystemConstant.UTF8_CODE);
                String url = request.getRequestURI();
                url = url.substring(request.getContextPath().length());
                if (!isAuth(url) || url.contains("/api/"))
                {
                    if (url.contains("/api/"))
                    {
                        
                    }
//                    {
//                        String responseJson = null;
//                        String userNameAuth = "";
//                        String responseAuth = "";
//                        String nonceAuth = "";
//                        String uriAuth = "";
//                        try
//                        {
//                            InputStream requestInputStream = request.getInputStream();
//                            String requestJson = IOUtils.toString(requestInputStream, "UTF-8");
//                            logger.info("HttpServer.doPost requestJson:[" + requestJson + "]");
//                            ObjectMapper mapper = new ObjectMapper();
//                            Map<String, Object> map = mapper.readValue(requestJson, Map.class);
//                            Object head = map.get(JSON_MSG_HEAD_KEY);
//                            MsgheadInfo msgHead = mapper.readValue(mapper.writeValueAsString(head), MsgheadInfo.class);
//                            String msgType = msgHead.getMsgType();
//                            String direction = msgHead.getDirection();
//                            String version = msgHead.getVersion();
//                            
//                            if (StringUtils.isBlank(msgType))
//                            {
//                                throw new Exception("msg type null.");
//                            }
//                            if (!HTTP_REQUEST.equals(direction))
//                            {
//                                throw new Exception("direction not correct.");
//                            }
//                            // 获取请求header中的nonceAuth
//                            String header = request.getHeader("Authorization");
//                            // 如果Authorization鉴权信息不为空
//                            if (null != header && !"".equals(header))
//                            {
//                                String[] headers = header.split("[,]");
//                                for (int i = 0; i < headers.length; i++)
//                                {
//                                    if (-1 != headers[i].indexOf("username"))
//                                    {
//                                        userNameAuth = headers[i].substring(headers[i].indexOf("=") + 1);
//                                    }
//                                    else if (-1 != headers[i].indexOf("response"))
//                                    {
//                                        responseAuth = headers[i].substring(headers[i].indexOf("=") + 1);
//                                    }
//                                    else if (-1 != headers[i].indexOf("nonce") && -1 == headers[i].indexOf("cnonce"))
//                                    {
//                                        nonceAuth = headers[i].substring(headers[i].indexOf("=") + 1);
//                                    }
//                                    else if (-1 != headers[i].indexOf("uri"))
//                                    {
//                                        uriAuth = headers[i].substring(headers[i].indexOf("=") + 1);
//                                    }
//                                }
//                                if (StringUtil.isNull(responseAuth) || StringUtil.isNull(userNameAuth)
//                                    || StringUtil.isNull(uriAuth) || StringUtil.isNull(nonceAuth))
//                                {
//                                    throw new CustomException();
//                                }
//                                else if (NONCE_MAP.containsKey(nonceAuth))
//                                {
//                                    // FileSystemXmlApplicationContext context =
//                                    // new
//                                    // FileSystemXmlApplicationContext("webRoot/WEB-INF/dubbo-consumer.xml");
//                                    // context.start();
//                                    // FileSystemXmlApplicationContext context =
//                                    // ApplicationContextUtils.getCurrentWebApplicationContext();
//                                    // WebApplicationContext context =
//                                    // ContextLoader.getCurrentWebApplicationContext();
//                                    // AdsLoginService service =
//                                    // (AdsLoginService)
//                                    // context.getBean("adsLoginService"); //
//                                    // 获取远程服务代理
//                                    // AdsLoginReq loginReq = new AdsLoginReq();
//                                    // loginReq.setNonceAuth(nonceAuth);
//                                    // loginReq.setUserNameAuth(userNameAuth);
//                                    // loginReq.setUriAuth(uriAuth);
//                                    // loginReq.setResponseAuth(responseAuth);
//                                    // AdsLoginResp loginResp =
//                                    // service.login(loginReq);
//                                    // if
//                                    // (SUCCESS_CODE.equals(loginResp.getResultCode()))
//                                    // {
//                                    // UserCache userCache =
//                                    // JsonParseUtil.json2Obj(loginResp.getUserInfoJson(),
//                                    // UserCache.class);
//                                    // EhCacheUtil.getCache().put(new
//                                    // Element(userNameAuth, userCache));
//                                    // EhCacheUtil.getCache().flush();
//                                    // if (null !=
//                                    // Env.getMsgTypeFuntion(msgType)) {
//                                    // FunctionInfo func =
//                                    // Env.getMsgTypeFuntion(msgType);
//                                    // Class<?> reqInfoClass =
//                                    // Class.forName(func.getReqClazz());
//                                    // AcRequestInfo reqBean = (AcRequestInfo)
//                                    // reqInfoClass.newInstance();
//                                    // AcRequestInfo requestInfo =
//                                    // JsonParseUtil.json2Obj(requestJson,
//                                    // reqBean.getClass());
//                                    // requestInfo.getMsgHead().setUserNameAuth(userNameAuth);
//                                    // Class<?> servletTypeClass =
//                                    // Class.forName(func.getServiceClazz());
//                                    // Method method =
//                                    // servletTypeClass.getMethod("getInstance",
//                                    // null);
//                                    // AbsAbilityCustomerHandler serviceProcess
//                                    // = (AbsAbilityCustomerHandler) method
//                                    // .invoke(servletTypeClass, null);
//                                    // AcResponseInfo respBean =
//                                    // serviceProcess.exec(requestInfo);
//                                    // MsgHead msghead =
//                                    // mapper.readValue(mapper.writeValueAsString(head),
//                                    // MsgHead.class);
//                                    // msghead.setDirection(HTTP_RESPONSE);
//                                    // respBean.setMsgHead(msghead);
//                                    // responseJson =
//                                    // JsonParseUtil.obj2Json(respBean);
//                                    // response.setStatus(200);
//                                    // NONCE_MAP.remove(nonceAuth);
//                                    // String nextnonce =
//                                    // RandomUtil.randomStringa(NONCE_LENGTH);
//                                    // NONCE_MAP.put(nextnonce, null);
//                                    // response.setHeader("Authentication-Info",
//                                    // "Digest realm=realm@host.com,nextnonce="
//                                    // + nextnonce + "");
//                                    // } else {
//                                    // throw new Exception("msg type invalid");
//                                    // }
//                                    // } else {
//                                    // NONCE_MAP.remove(nonceAuth);
//                                    // String nonce =
//                                    // RandomUtil.randomStringa(NONCE_LENGTH);
//                                    // String opaque =
//                                    // RandomUtil.randomStringa(OPAQUE_LENGTH);
//                                    // response.setHeader("WWW-Authenticate",
//                                    // "Digest
//                                    // realm=realm@host.com,qop=auth,nonce=" +
//                                    // nonce + ",opaque="
//                                    // + opaque + "");
//                                    // NONCE_MAP.put(nonce, null);
//                                    // response.setStatus(401);
//                                    // msgHead.setDirection(HTTP_RESPONSE);
//                                    // ResultInfo info = new ResultInfo();
//                                    // info.setResultCode(loginResp.getResultCode());
//                                    // info.setResultDesc(loginResp.getResultDesc());
//                                    // Map<String, Object> respMap = new
//                                    // HashMap<String, Object>();
//                                    // respMap.put(JSON_MSG_HEAD_KEY, msgHead);
//                                    // respMap.put(JSON_RESULT_KEY, info);
//                                    // responseJson =
//                                    // mapper.writeValueAsString(respMap);
//                                    // }
//                                    result = true;
//                                }
//                                // 第一次鉴权，直接返回401
//                                else
//                                {
//                                    throw new CustomException();
//                                }
//                            }
//                            // 如果Authorization鉴权信息为空，直接返回401
//                            else
//                            {
//                                throw new CustomException();
//                            }
//                        }
//                        catch (CustomException e)
//                        {
//                            logger.error("HttpServer CustomException", e);
//                            responseJson = buildException(response, nonceAuth, "1001", "Auth fail.");
//                        }
//                        catch (Exception e)
//                        {
//                            logger.error("HttpServer Exception", e);
//                            responseJson = buildException(response, nonceAuth, "1002", e.getMessage());
//                        }
//                        finally
//                        {
//                            if (result)
//                            {
//                                logger.info("HttpServer.doPost responseJson:[" + responseJson + "]");
//                                response.setContentType("text/html");
//                                response.setCharacterEncoding("UTF-8");
//                                PrintWriter out = response.getWriter();
//                                out.println(responseJson);
//                                out.flush();
//                                out.close();
//                            }
//                        }
//                        // request.setAttribute("jsonData","sadfasdfasdfadfasdf");
//                        // UserInfo userInfo = new UserInfo();
//                        // userInfo.setId(1);
//                        // userInfo.setUserName("yizhichao");
//                        // userInfo.setPassWord("123456");
//                        // request.setAttribute("jsonData",userInfo);
//                        // System.out.println("sssssssssssssssssss");
//                    }
                }
                return result;
            }
            
        }).addPathPatterns("/**").excludePathPatterns("/login");
    }
    
    private String buildException(HttpServletResponse response, String nonceAuth, String resultCode, String resultDesc)
    {
        String responseJson;
        NONCE_MAP.remove(nonceAuth);
        String nonce = RandomUtil.randomStringa(NONCE_LENGTH);
        String opaque = RandomUtil.randomStringa(OPAQUE_LENGTH);
        response.setHeader("WWW-Authenticate",
            "Digest realm=realm@host.com,qop=auth,nonce=" + nonce + ",opaque=" + opaque + "");
        NONCE_MAP.put(nonce, null);
        response.setStatus(401);
        responseJson =
            "{\"MsgHead\":{\"version\":\"3.0\",\"direction\":\"response\",\"msgType\":\"COMMON\"},\"Result\":{\"resultCode\":\""
                + resultCode + "\",\"resultDesc\":\"" + resultDesc + "\"}}";
        return responseJson;
    }
}
