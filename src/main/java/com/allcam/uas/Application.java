package com.allcam.uas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;

/**
 * 
 * <一句话功能简述> <功能详细描述>
 * 
 * @author yizhichao
 * @version [版本号, 2016年7月12日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@SpringBootApplication
@ImportResource({"classpath:applicationContext-dubbo.xml"}) //加入spring的bean的xml文件   
// same as @Configuration @EnableAutoConfiguration
// @Configuration
// @ComponentScan
// @EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {
	// private static final Logger logger =
	// LoggerFactory.getLogger(Application.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) {
		// File file = new File("log4j2.xml");
		// BufferedInputStream in = null;
		// try
		// {
		// in = new BufferedInputStream(new FileInputStream(file));
		// final ConfigurationSource source = new ConfigurationSource();
		// source.setInputStream(in);
		// Configurator.initialize(null, source);
		// }
		// catch (FileNotFoundException e)
		// {
		// e.printStackTrace();
		// }
		SpringApplication.run(Application.class, args);
	}

	// @Configuration
	// static class WebMvcConfigurer extends WebMvcConfigurerAdapter {
	//
	// public void addInterceptors(InterceptorRegistry registry) {
	// registry.addInterceptor(new HandlerInterceptorAdapter() {
	//
	// @Override
	// public boolean preHandle(HttpServletRequest request, HttpServletResponse
	// response, Object handler)
	// throws Exception {
	// request.getContextPath();
	// System.out.println("11111111111
	// Application.WebMvcConfigurer.interceptor");
	// return true;
	// }
	// }).addPathPatterns("/**");
	// }
	// }

}