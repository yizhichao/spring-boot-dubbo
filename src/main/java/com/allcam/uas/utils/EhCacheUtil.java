package com.allcam.uas.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.allcam.utils.Env;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

/**
 * EhCache工具类
 * 
 * @author marui
 * @version [版本号, 2015-5-18]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class EhCacheUtil
{
    /** 定义日志对象 */
    public static final Log LOG = LogFactory.getLog(EhCacheUtil.class);
    
    private static CacheManager cacheManager = null;
    static
    {
        System.setProperty("net.sf.ehcache.enableShutdownHook", "true");
        cacheManager = CacheManager.create(Env.getWebPath() + "WEB-INF"
            + System.getProperty("file.separator") + "conf" + System.getProperty("file.separator") + "ehcache.xml");
    }
    
    public static Element getCachedElement(String cacheName, String key)
    {
        Element elem = getCache(cacheName).get(key);
        return (null == elem || elem.isExpired()) ? null : elem;
    }
    
    public static Ehcache getCache(String cacheName)
    {
        Ehcache ehcache = cacheManager.getEhcache(cacheName);
        return ehcache;
    }
}
